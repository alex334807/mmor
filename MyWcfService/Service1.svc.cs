﻿using System;
using System.Collections.Generic;
using MyWcfService.Models;

namespace MyWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        BLL bll = new BLL();

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public HomeAccInfo GetHomePageInf(string token)
        {
            var a = bll.GetHomePageInf(token);
            return a;
        }

        public HomeAccInfo CreateGroup(string token, string groupName)
        {
            return bll.CreateGroup(token, groupName);
        }

        /// <summary>
        /// Передаёт запрос пользователя, который хочет зайти или зарегестрироваться
        /// </summary>
        string IService1.LogInInformation(string login, string password, bool trySignIn)
        {
            return bll.SetInfo(login,password,trySignIn);
        }

        public string DeleteGroup(string token, int groupId)
        {
            return bll.DeleteGroup(token, groupId);
        }

        public string RenameGroup(string token, int groupId, string newName)
        {
            return bll.RenameGroup(token, groupId, newName);
        }

        public string CreateAccount(string token, int groupId)
        {
            return bll.CreateAccount(token, groupId);
        }

        public string[] GetAllUsersOfGroup(string token, int groupId)
        {
            return bll.GetAllUsersOfGroup(token, groupId);
        }

        public EntityAllUsersOfGroup GetAllAccountsOfGroup(string token, int groupId)
        {
            return bll.GetAllAccoutsOfGroup(token, groupId);
        }

        public string AddRequestOnAddingUserInGroup(string token, int groupId, string userLogin)
        {
            return bll.AddInRequestsOnAddInGroups(token, groupId, userLogin);
        }

        public string ResponseOnInvite(string token, int inviteId, bool acceptOrDecline)
        {
            return bll.AnswerOnAddingInGroup(token, inviteId, acceptOrDecline);
        }

        public ActionsWithAccount GetAllActionsOfAccount(string token, int accountId)
        {
            throw new NotImplementedException();
        }

        public string AddActionWithAccount(string token, int idOfOperation, decimal money, int idOfAccount, string comment)
        {
            throw new NotImplementedException();
        }

        public UserInvites GetAllUserInvites( string token )
        {
            return bll.GetAllUserInvites(token);
        }

        public HistoryAndTypes GetHistoryAndTypes(string token, string moneyAccountId)
        {
            return bll.ReturnActionsHistoryAndTypes(token, moneyAccountId);
        }

        public string DoOperation(string token, int moneyAccId, string operationType, decimal money, string comment)
        {
            return bll.DoAction( token, moneyAccId, operationType,  money,  comment);
        }
    }
}
