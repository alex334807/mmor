﻿using System;
using System.Collections.Generic;
using DAL;
using DAL.Entities;
using MyWcfService.Models;

namespace MyWcfService
{
    public class BLL
    {
        private string[] tokenPieces =  { "hrw","pht","ltr","bvf","snm","lhj","zdr", "234" , "642", "94", "54", "21", "98", "0" };
        private Random rnd = new Random();
        private List<string> emptyList = new List<string>();
        private const string tooShortOrTooLong = "4";

        /// <summary>
        /// Сущность, для работы с dal слоем
        /// </summary>
        private DALManager dManager = new DALManager();

        public BLL()
        {

        }

        /// <summary>
        /// В зависимости от посупившей информации регестрирует или выдаёт токен зарегестрированному пользователю
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="trySignIn"></param>
        /// <returns></returns>
        public string SetInfo(string login, string password, bool trySignIn)
        {
            if (trySignIn)
            {
                return Logging(login, password);
            }
            else
            {
                return Registration(login, password);
            }
        }

        /// <summary>
        /// Находит всю информацию для страницы АККАУНТ, что доступно пользователю
        /// </summary>
        /// <returns></returns>
        public HomeAccInfo GetHomePageInf(string token)
        {
            int userId;
            string userName;
            userName = dManager.ReturnUserNameByToken(token);
            userId = dManager.ReturnIdByUserName(userName);

            ///Получаю айди всех групп пользователя по айди пользователя
            List<int> groupsId = dManager.GetAllIdOfGroupsById(userId);
            
            var homeAccInfo = new HomeAccInfo();
            homeAccInfo.ListGroupArrElem = new List<GroupsArrElem>();
            var dbInfo =  dManager.ReturnInfo(token);
            
            ///Получаю все счета пользователя по айди пользователя
            List<string> accountsOfUser = dManager.GetAllAccountsById(userId);
            homeAccInfo.Accounts = accountsOfUser;

            ///Получаю все группы пользоваетля по айди пользователя
            List<string> groupsOfUser = dManager.GetAllGroupsById(userId);
            for (var counterId = 0; counterId < groupsId.Count; counterId++)
            {
                homeAccInfo.ListGroupArrElem.Add(new GroupsArrElem() { id = groupsId[counterId], group = groupsOfUser[counterId] });
            }

            ///Проверяю есть ли запросы на добаление этого пользователя в группу
            homeAccInfo.IsGroupRequestExist = dManager.CheckGroupRequests(userId);

            return homeAccInfo;
        }

        /// <summary>
        /// Возвращает заполненый объект, содержащий группу и аккаунты этой группы пользователя и счета этой группы
        /// </summary>
        /// <param name="token"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        internal EntityAllUsersOfGroup GetAllAccoutsOfGroup(string token, int groupId)
        {
            ///Получаю имя пользователя по токену (проверяю, что он авторизован)
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                /// Получаю айди пользователя, для нахождения всех его групп
                var userId = dManager.ReturnIdByUserName(userName);
                /// Получаю все айди групп пользователя
                var allGroupsIdOfThisUser = dManager.GetAllIdOfGroupsById(userId);
                /// Создаю объект для возвращения из метода
                var accEntity = new EntityAllUsersOfGroup();
                /// Задаю переменную для отслеживания нахождения
                var found = false;
                /// Прохожусь циклом по всем айди групп пользователя
                foreach (var group in allGroupsIdOfThisUser)
                {
                    /// если хоть один айди  совпадает с предложенным айди группы, то
                    if (group == groupId)
                    { 
                        ///Изменяю переменную на (правду) найдено
                        found = true;
                    }
                }
                /// Если найдена тот айди группы, что пришёл в метод, то
                if (found)
                {
                    /// Заполняю в объект для возвращения групп пользователя
                    accEntity.AllUsersOfGroup = GetAllUsersOfGroup(token, groupId);
                    /// Создаю массив для записи количества денег из всех счетов клиентов
                    //var accountsMoney = dManager.ReturnAllAccountsOfThisGroup(groupId);
                    var accEnt = dManager.GetIdAndMoney(groupId);

                    accEntity.AllAccountsOfGroup = new AccountEntity[accEnt.Length];
                    var counter = 0;
                    ///из ДАЛ описания сущности переписать в сущность сервиса, для передачи
                    foreach (var elem in accEnt)
                    {
                        
                        accEntity.AllAccountsOfGroup[counter] = new AccountEntity { accountId = elem.accountId, accountMoney = elem.accountMoney };
                        counter++;
                    }

                    /*
                    наполнить allAccountsOfGroup сразу и количеством денег на счёте и айди счёта
                    // нужен метод для наполнения сразу и количества денег и айди счетов

                      public AccountId: number;
                      public AccountMoney: number;
                    */

                    //accEntity.AllAccountsOfGroup
                    return accEntity;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        internal string DoAction(string token, int moneyAccId, string operationType, decimal money, string comment)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                var userId = dManager.ReturnIdByUserName(userName);
                ///Проверить что пользователь имеет доступ к этому счёту
                if(dManager.IsThatMoneyAccInGroupsOfThisUser(userId, moneyAccId))
                {
                    ///Выяснить операция это пополнения или снятия
                    bool isThatInvokeOperation = dManager.IsThatOperationInvoke(operationType);
                    if (isThatInvokeOperation)
                    {
                        ///изменить количество денег на счету в соответствии с операцией
                        dManager.ChangeMoneyOnMoneyAcc(moneyAccId, money, "+");
                    }
                    else
                    {
                        dManager.ChangeMoneyOnMoneyAcc(moneyAccId, money, "-");
                    }
                    /// внести операцию в историю операций
                    int operationTypeId = dManager.ReturnOperationIdByOperationName(operationType);
                    dManager.MakeItHistory(operationTypeId, money, moneyAccId, userId, comment);
                }
            }
            return null;
        }

        /// <summary>
        /// Проверяет доступ пользователя и возвращает (количество денег на счету, существующие типы, все действия, проведённые по счёту)
        /// </summary>
        /// <param name="token"></param>
        /// <param name="moneyAccId"></param>
        /// <returns></returns>
        internal HistoryAndTypes ReturnActionsHistoryAndTypes(string token, string moneyAccId)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                var objForReturn = new HistoryAndTypes();
                var userId = dManager.ReturnIdByUserName(userName);
                var intMoneyAccId = int.Parse(moneyAccId);

                ///Проверяю может ли этот пользователь иметь доступ к этому счёту
                if (dManager.IsThatMoneyAccInGroupsOfThisUser(userId, intMoneyAccId))
                {
                    ///Нахожу и складываю все типы операций
                    var allActionsType = dManager.ReturnAllActionsType();
                    objForReturn.TypesOfActionEntity = TranslateUserActionTypeToActionEntity(allActionsType);
                    ///Нахожу и складываю количество денег на этом счету
                    var moneyOnAccount = dManager.ReturnMoneyOnMoneyAcc(intMoneyAccId);
                    objForReturn.Money = moneyOnAccount;
                    ///Нахожу историю действий по данному счёту
                    var historyOfActions = dManager.ReturnMoneyAccHistory(intMoneyAccId);
                    ActionEntity[] translatedArray = TranslateUserActionToActionEntity(historyOfActions);
                    objForReturn.ActionHistory = translatedArray;

                    return objForReturn;
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Переводит массив UserActionType в объект TypesOfActionEntity
        /// </summary>
        /// <param name="allActionsType"></param>
        /// <returns></returns>
        private TypesOfActionEntity TranslateUserActionTypeToActionEntity(UserActionType[] allActionsType)
        {
            var typesOfActionEntity = new TypesOfActionEntity();
            //typesOfActionEntity.Invoke = new string[allActionsType.Length];
            //typesOfActionEntity.NotInvoke = new string[allActionsType.Length];
            var invokeList = new List<string>();
            var notInvokeList = new List<string>();
            var invokeCounter = 0;
            var notInvokeCounter = 0;

            ///Наполняю листы для последующего преобразования в массив
            foreach (var userActionType in allActionsType)
            {
                if (userActionType.isInvoke)
                {
                    invokeList.Add( userActionType.TypeOfAction);
                }
                else
                {
                    notInvokeList.Add(userActionType.TypeOfAction);
                }
            }

            typesOfActionEntity.Invoke = new string[invokeList.Count];
            ///Наполняю массив Invoke
            foreach (var elem in invokeList)
            {
                typesOfActionEntity.Invoke[invokeCounter] = elem;
                invokeCounter++;
            }

            typesOfActionEntity.NotInvoke = new string[notInvokeList.Count];
            ///Наполняю массив NotInvoke
            foreach (var elem in notInvokeList)
            {
                typesOfActionEntity.NotInvoke[notInvokeCounter] = elem;
                notInvokeCounter++;
            }

            return typesOfActionEntity;
        }

        /// <summary>
        /// Возвращает все приглашения пользователя по токену
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        internal UserInvites GetAllUserInvites(string token)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                //var InvitesArrForResponse = new List<InviteElement>();
                var userInvitesForResponse = new UserInvites();
                userInvitesForResponse.InviteElements = new List<InviteElement>();
                //var namesOfGroups = new List<string>();
                //var namesOfRequesters = new List<string>();
                var userId = dManager.ReturnIdByUserName(userName);
                ///Сущность всех приглашений пользователя
                var userInvites = dManager.ReturnUserInvitesById(userId);
                string requesterName;
                string groupName;
                ///Для всех приглашений этого пользователя записываю их в массив имён групп и массив приглашающих
                foreach (var elem in userInvites)
                {
                    requesterName = dManager.ReturnUserNameById(elem.IdUserWhoCreateRequest);
                    groupName = dManager.ReturnGroupNameById(elem.IdGroup);
                    userInvitesForResponse.InviteElements.Add(new InviteElement() { loginOfUserWhoCreateReq = requesterName, nameOfGroup = groupName, inviteId = elem.Id});
                    
                }
                
                return userInvitesForResponse;
            }
            return null;
        }

        internal string[] GetAllUsersOfGroup(string token, int groupId)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                var userId = dManager.ReturnIdByUserName(userName);
                var allGroupsIdOfThisUser = dManager.GetAllIdOfGroupsById(userId);
                var found = false;
                foreach (var group in allGroupsIdOfThisUser)
                {
                    if(group == groupId)
                    {
                        found = true;
                    }
                }
                if (found)
                {
                    return dManager.ReturnAllUsersOfThisGroup(groupId);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Проводит проверку, что пользователь, который запрашивает переименование состоит в этой группе и переименует её
        /// </summary>
        /// <param name="token"></param>
        /// <param name="groupId"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        internal string RenameGroup(string token, int groupId, string newName)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                var allGroupOfUser = dManager.GetAllIdOfGroupsById( dManager.ReturnIdByUserName(userName) );
                var foundInList = false;
                foreach(var userGroupId in allGroupOfUser)
                {
                    if(userGroupId == groupId)
                    {
                        foundInList = true;
                    }
                }
                if (foundInList)
                {
                    dManager.RenameGroup(groupId, newName);
                    return "Группа была переименована";
                }
                else
                {
                    return "Вы не можете переименовывать эту группу";
                }
            }
            else
            {
                return "Вы не вошли в систему";
            }
        }

        /// <summary>
        /// Проверяет права доступа и удаляет группу
        /// </summary>
        /// <param name="token"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        internal string DeleteGroup(string token, int groupId)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if(userName != null)
            {
                int idOfRealOwnerOfGroup = dManager.ReturnGroupOwnerById(groupId);
                int idOfImagineOwner = dManager.ReturnIdByUserName(userName);

                if (idOfRealOwnerOfGroup == idOfImagineOwner)
                {
                    //dManager.DeleteAllOperationsOfAllAccountsInGroup(groupId);
                    //dManager.DeleteAllAccountsByGroupId(groupId);
                    
                    dManager.DeleteOperationsAndAccountsByGroupId(groupId);
                    dManager.DeleteAllInvitesByGroupId(groupId);
                    return ("Группа" + dManager.DeleteGroup(groupId) + "удалена");
                }
                else
                {
                    return "У вас нет доступа";
                }
            }
            else
            {
                return "Вы не вошли в систему";
            }
        }

        /// <summary>
        /// Создаёт новую группу пользователю
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public HomeAccInfo CreateGroup(string token, string groupName)
        {
            ///Имя пользователя, который будет владельцем группы и который будет первым в неё добавлен
            var userName = dManager.ReturnUserNameByToken(token) ;
            ///Id пользователя с которым работаем
            var userId = dManager.ReturnIdByUserName(userName) ;
            ///Id только что созданной группы, для добавления в неё первого пользователя
            dManager.CreateGroupAndAddOwner(groupName, userId);
            
            return GetHomePageInf(token);
        }

        /// <summary>
        /// Создаёт новый счёт
        /// </summary>
        /// <returns></returns>
        public string CreateAccount(string token, int groupId)
        {
            var userName = dManager.ReturnUserNameByToken(token);
            if (userName != null)
            {
                var allGroupOfUser = dManager.GetAllIdOfGroupsById(dManager.ReturnIdByUserName(userName));
                var foundInList = false;
                foreach (var userGroupId in allGroupOfUser)
                {
                    if (userGroupId == groupId)
                    {
                        foundInList = true;
                    }
                }
                if (foundInList)
                {
                    dManager.CreateNewAccount(groupId);
                    return "Добавил новый счёт";
                }
                else
                {
                    return "Вы не можете добавить  счёт в эту группу";
                }
            }
            else
            {
                return "Вы не вошли в систему";
            }

        }

        /// <summary>
        /// Метод входа в свой аккаунт, возвращает token
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        private string Logging(string login, string password)
        {
            if (login.Length < 20 && login.Length > 4 && password.Length < 20 && password.Length > 4)
            {
                var userId = dManager.ReturnIdByUserName(login);
                if(userId > 0)
                {
                    var TruePasswordHash = dManager.ReturnPasswordHashByUserId(userId);
                    var HashForChecking = MakeItHash(password);
                    if (TruePasswordHash == HashForChecking)
                    {
                        return dManager.AddTokenForUser(login, GenerateToken());
                    }
                    else
                    {
                        return "Отказано";
                    }
                }
                else
                {
                    return "Отказано";
                }
            }
            else
                return tooShortOrTooLong;
        }

        /// <summary>
        /// Выдать все аккаунты
        /// </summary>
        /// <param name="userLogin"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        internal List<string> GetAccounts(string userLogin, string token)
        {
            //Тут надо ещё проверять логин

            if (someCheck(userLogin) && someCheck(token))
            {
                return dManager.GetAccounts(userLogin);
            }
            else
            {
                return emptyList;
            }
        }

        /// <summary>
        /// Добавляет (запрос на добавление в группу) в таблицу, после проверок
        /// </summary>
        /// <param name="token"></param>
        /// <param name="groupId"></param>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        internal string AddInRequestsOnAddInGroups(string token, int groupId, string userLoginForAddInGroup)
        {
            //проверяю, что пользователь, который создаёт запрос авторизован
            var userWhoWantAdd = dManager.ReturnUserNameByToken(token);
            if(userWhoWantAdd == null)
            {
                return "Что-то не так с вашей авторизацией, попробуйте перезагрузить страницу или перезайти в аккаунт";
            }
            var idOfUserWhoCreateRequest = dManager.ReturnIdByUserName(userWhoWantAdd);

            if ( !dManager.DoesThisLoginExist(userLoginForAddInGroup) )
            {
                return "Вы ошиблись в имени того, кого хотите пригласить";
            }
            var userIdForAddInGroup = dManager.ReturnIdByUserName(userLoginForAddInGroup);

            if( dManager.CheckIsThisUserInGroup(idOfUserWhoCreateRequest, groupId) && !dManager.CheckIsThisUserInGroup(userIdForAddInGroup, groupId) )
            {
                dManager.AddInRequestsOnAddInGroups(idOfUserWhoCreateRequest, groupId, userIdForAddInGroup);
            }
            else
            {
                return "Ошибка. Возможно пользователь уже состоит в этой группе.";
            }

            
            return "Запрос добавлен!";
        }

        internal string AnswerOnAddingInGroup(string token, int inviteId, bool isAccepted)
        {
            ///Проверяю, что пользователь, который отвечает на запрос авторизирован
            var invitedUserName = dManager.ReturnUserNameByToken(token);
            int invitedUserId = dManager.UserIdByName(invitedUserName);

            if(invitedUserName != null)
            {
                ///Проверяю существует ли такое приглашение
                var isInviteExist = dManager.IsInviteExist(inviteId, invitedUserId);

                if ( isInviteExist )
                {
                    if (isAccepted)
                    {
                        dManager.AddUserInGroup(dManager.ReturnGroupIdByInviteId(inviteId), invitedUserId);
                        dManager.DeleteInvite(inviteId);
                        return "Вы вступили в группу";
                    }
                    else
                    {
                        dManager.DeleteInvite(inviteId);
                        return "Приглашение отменено";
                    }
                }
                else
                {
                    return "Данного приглашения не существует";
                }

            }
            else
            {
                return "Вы не авторизировались!";
            }

            
        }

        /// <summary>
        /// Метод регистрации
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string Registration(string login, string password)
        {
            if (login.Length < 20 && login.Length > 4 && password.Length < 20 && password.Length > 4)
            {
                return dManager.AddNewUserIfCan(login, MakeItHash(password));    
            }
            else
                return tooShortOrTooLong;

        }

        private bool CheckToken(string token, string login)
        {
            if (token.Equals(dManager.GetToken(login, token)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Делает из предложенного пароля стринговый хэш
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private string MakeItHash(string password)
        {
            var intedHashPassword = password.GetHashCode();
            string stringedHashPassword = intedHashPassword.ToString();
            return stringedHashPassword;
        }

        /// <summary>
        /// Обстрактная имитация проверки
        /// </summary>
        /// <param name="some"></param>
        /// <returns></returns>
        private bool someCheck(string some)
        {
            if (some.Length > 4 && some.Length < 20)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Создаёт рандомный токен, проверяет что он уникальный и возвращает его
        /// </summary>
        /// <returns></returns>
        private string GenerateToken()
        {
            string newToken = "";

            while (true)
            {
                for (var i = 0; i < 5; i++)
                {
                    var rndNum = rnd.Next(0, tokenPieces.Length);
                    newToken = newToken + tokenPieces[rndNum];
                }
                //проверить, что метод работает
                if (dManager.CheckTokenTable(newToken))
                {
                    return newToken;
                }
            }
        }

        private ActionEntity[] TranslateUserActionToActionEntity(DAL.Entities.UserAction[] userActions)
        {
            var translatedArray = new ActionEntity[userActions.Length];
            var counter = 0;

            foreach (var elem in userActions)
            {
                translatedArray[counter] = new ActionEntity();
                translatedArray[counter].Money = elem.Money;
                translatedArray[counter].TimeOperation = elem.DateOfOpertion.ToString();
                translatedArray[counter].Comment = elem.Comment;
                translatedArray[counter].OperationType = dManager.GetOperationName(elem.ActionTypeId);
                translatedArray[counter].UserName = dManager.ReturnUserNameById(elem.UserId);
                translatedArray[counter].IsInvoke = dManager.ReturnInvokeOrNot(elem.ActionTypeId);
                counter++;
            }
            return translatedArray;
        }
    }
}
