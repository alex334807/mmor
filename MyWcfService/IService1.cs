﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MyWcfService.Models;

namespace MyWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        string LogInInformation(string login, string password, bool trySignIn);

        [OperationContract]
        HomeAccInfo GetHomePageInf(string token);

        [OperationContract]
        HomeAccInfo CreateGroup(string token, string groupName);

        [OperationContract]
        string DeleteGroup(string token, int groupId);

        [OperationContract]
        string RenameGroup(string token, int groupId, string newName);

        [OperationContract]
        string CreateAccount(string token, int groupId);

        [OperationContract]
        string[] GetAllUsersOfGroup(string token, int groupId);

        [OperationContract]
        EntityAllUsersOfGroup GetAllAccountsOfGroup(string token, int groupId);

        [OperationContract]
        string AddRequestOnAddingUserInGroup(string token, int groupId, string userLogin);

        [OperationContract]
        string ResponseOnInvite(string token, int inviteId, bool acceptOrDecline);

        [OperationContract]
        ActionsWithAccount GetAllActionsOfAccount(string token, int accountId);

        [OperationContract]
        string AddActionWithAccount(string token, int idOfOperation, decimal money, int idOfAccount, string comment);

        [OperationContract]
        UserInvites GetAllUserInvites(string token);

        [OperationContract]
        HistoryAndTypes GetHistoryAndTypes(string token, string moneyAccountId);

        [OperationContract]
        string DoOperation(string token, int moneyAccId, string operationType, decimal money, string comment);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    [DataContract (IsReference = true)]
    public class HomeAccInfo
    {
        List<string> accounts;
        List<GroupsArrElem> idAndGroups;
        bool isGroupRequestExist;

        [DataMember()]
        public List<string> Accounts
        {
            get { return accounts; }
            set { accounts = value; }
        }

        [DataMember()]
        public List<GroupsArrElem> ListGroupArrElem
        {
            get { return idAndGroups; }
            set { idAndGroups = value; }
        }

        [DataMember()]
        public bool IsGroupRequestExist
        {
            get { return isGroupRequestExist; }
            set { isGroupRequestExist = value; }
        }

    }

    [DataContract (IsReference = true)]
    public class UserInvites
    {
        List<InviteElement> invitesElement;

        [DataMember()]
        public List<InviteElement> InviteElements
        {
            get { return invitesElement; }
            set { invitesElement = value; }
        }
    }

    [DataContract(IsReference = true)]
    public class EntityAllUsersOfGroup
    {
        string[] allUserOfGroup;
        AccountEntity[] allAccountsOfGroup;

        [DataMember()]
        public string[] AllUsersOfGroup
        {
            get { return allUserOfGroup; }
            set { allUserOfGroup = value; }
        }

        [DataMember()]
        public AccountEntity[] AllAccountsOfGroup
        {
            get { return allAccountsOfGroup; }
            set { allAccountsOfGroup = value; }
        }
    }

    [DataContract(IsReference = true)]
    public class AccountEntity
    {
        public int accountId;
        public decimal accountMoney;

        [DataMember()]
        public int AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }

        [DataMember()]
        public decimal AccountMoney
        {
            get { return accountMoney; }
            set { accountMoney = value; }
        }
    }

    [DataContract (IsReference = true)]
    public class HistoryAndTypes
    {
        decimal money;
        ActionEntity[] actionHistory;
        TypesOfActionEntity typesOfActionEntity;

        [DataMember()]
        public decimal Money { get => money; set => money = value; }

        [DataMember()]
        public ActionEntity[] ActionHistory { get => actionHistory; set => actionHistory = value; }

        [DataMember()]
        public TypesOfActionEntity TypesOfActionEntity { get => typesOfActionEntity; set => typesOfActionEntity = value; }
    }

    [DataContract (IsReference = true)]
    public class ActionEntity
    {
        decimal money;
        string timeOperation;
        string userName;
        string comment;
        string operationType;
        string isInvoke;

        [DataMember()]
        public string IsInvoke        { get => isInvoke; set => isInvoke = value; }

        [DataMember()]
        public decimal Money          { get => money; set => money = value; }

        [DataMember()]
        public string TimeOperation   { get => timeOperation; set => timeOperation = value; }

        [DataMember()]
        public string UserName        { get => userName; set => userName = value; }

        [DataMember()]
        public string Comment         { get => comment; set => comment = value; }

        [DataMember()]
        public string OperationType   { get => operationType; set => operationType = value; }
    }

    [DataContract (IsReference = true)]
    public class TypesOfActionEntity
    {
        private string[] invoke;
        private string[] notInvoke;

        [DataMember()]
        public string[] Invoke { get => invoke; set => invoke = value; }

        [DataMember()]
        public string[] NotInvoke { get => notInvoke; set => notInvoke = value; }
    }
    
}
