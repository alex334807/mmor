﻿using System.Collections.Generic;

namespace MyWcfService
{
    internal class GroupsAndId
    {
        public List<string> groupsOfUser { get; set; }
        public List<int> groupsId { get; set; }
        public List<string> accountsOfUser { get; set; }
    }
}