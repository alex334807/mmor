﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class Token
    {
        public int Id { get; set; }

        /// <summary>
        /// Токен, который выдаётся пользователю
        /// </summary>
        public string TokenName { get; set; }

        /// <summary>
        /// Пользователь, которому был выдан токен
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Время в которое токен был выдан
        /// </summary>
        public DateTime TokenWasAuthorized { get; set; }
    }
}
