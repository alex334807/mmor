﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWcfService.Models
{
    public class InviteElement
    {
        /// <summary>
        /// Имя группы в которую приглашают пользователя
        /// </summary>
        public string nameOfGroup { get; set; }
        /// <summary>
        /// Логин пользователя, который приглашает в группу
        /// </summary>
        public string loginOfUserWhoCreateReq { get; set; }
        /// <summary>
        /// айди приглашения для ответа на него
        /// </summary>
        public int inviteId { get; set; }
    }
}