﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWcfService.Models
{
    public class SignInSignUpJson
    {
        public string login { get; set; }
        public string pass { get; set; }
        public bool trySignIn { get; set; }
    }
}