﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Models
{
    /// <summary>
    /// Сущность для хранения логина - пароля пользователя
    /// </summary>
    public class LoginPassword
    {
        public int Id { get; set; }
        /// <summary>
        /// Айди пользователя
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Хэш пароля пользователя
        /// </summary>
        public string Password { get; set; }
    }
}
