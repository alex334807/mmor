﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWcfService.Models
{
    /// <summary>
    /// Сущность для хранения названия группы и айди её владельца
    /// </summary>
    public class NameOfGroup
    {
        public int Id { get; set; }
        /// <summary>
        /// Имя группы
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// Айди владельца группы с таким названием
        /// </summary>
        public int OwnersId { get; set; }
    }
}