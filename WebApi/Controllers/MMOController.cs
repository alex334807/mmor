﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;
using WebApi.ServiceReference1;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MMOController : ApiController
    {
        Service1Client MyService = new Service1Client();

        // GET api/values
        public string[] Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "Дарова";
        }

        // POST api/values
        [HttpPost]
        public HttpResponseMessage signInSignUp(SignInSignUpJson body)
        {
            return Request.CreateResponse(HttpStatusCode.Accepted, MyService.LogInInformation(body.login, body.password, body.trySignIn));
        }


        // PUT api/values/5
        public void Put([FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [HttpPost]
        public ServiceReference1.HomeAccInfo GetAccInfo(AccInfRequest body)
        {
            var request = MyService.GetHomePageInf(body.token);
            return request;
        }

        [HttpPost]
        public ServiceReference1.HomeAccInfo CreateGroup(newGroupReq req)
        {
            var request = MyService.CreateGroup(req.token, req.nameForGroup);
            return request;
        }

        [HttpPost]
        public HttpResponseMessage DeleteGroup(frontRequest request)
        {
            return Request.CreateResponse(HttpStatusCode.OK, MyService.DeleteGroup(request.Token, request.GroupId));
        }

        [HttpPost]
        public HttpResponseMessage AddAccount(frontRequest request)
        {
            return Request.CreateResponse(HttpStatusCode.Created, MyService.CreateAccount(request.Token, request.GroupId));
        }

        [HttpPost]
        public HttpResponseMessage RenameGroup(renameGroupReq request)
        {
            return Request.CreateResponse(HttpStatusCode.OK, MyService.RenameGroup(request.Token, request.GroupId, request.NewName));
        }

        [HttpPost]
        public ServiceReference1.EntityAllUsersOfGroup GetAllUsersOfGroup(frontRequest request)
        {
            var responseForFront = MyService.GetAllAccountsOfGroup(request.Token, request.GroupId);
            
            return responseForFront;
        }

        [HttpPost]
        public string AddRequestOnAddingUserInGroup(renameGroupReq addRequestOnAddingUserInGroup)
        {
            return MyService.AddRequestOnAddingUserInGroup(addRequestOnAddingUserInGroup.Token, addRequestOnAddingUserInGroup.GroupId, addRequestOnAddingUserInGroup.NewName);
        }

        [HttpPost]
        public UserInvites GetAllUserInvites(AccInfRequest request)
        {
            var a = MyService.GetAllUserInvites(request.token);
            return a;
        }

        [HttpPost]
        public string ResponseOnInvite(InviteResponse inviteRerponse)
        {
            return MyService.ResponseOnInvite(inviteRerponse.token, inviteRerponse.InviteId, inviteRerponse.isAccepted);
        }

        [HttpPost]
        public HistoryAndTypes GetHistoryAnfTypes(historyAndTypesRequest request)
        {
            var a = MyService.GetHistoryAndTypes(request.token, request.moneyAccId);
            return a;
        }

        [HttpPost]
        public string DoOperation(ActionRequest request)
        {
            return MyService.DoOperation(request.token, request.moneyAccId, request.operationType, request.money, request.comment);
        }
    }
}