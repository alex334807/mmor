﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class ActionRequest
    {
        public int moneyAccId { get; set; }
        public string token { get; set; }
        public string operationType { get; set; }
        public decimal money { get; set; }
        public string comment { get; set; }
    }
}