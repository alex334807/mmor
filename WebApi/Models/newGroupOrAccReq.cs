﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class newGroupReq
    {
        public string token { get; set; }
        public string nameForGroup { get; set; }
    }
}