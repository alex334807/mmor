﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class renameGroupReq
    {
        public string Token { get; set; }
        public int GroupId { get; set; }
        public string NewName { get; set; }
    }
}