﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class InviteResponse
    {
        /// <summary>
        /// Токен пользователя
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// Айди приглашения
        /// </summary>
        public int InviteId { get; set; }
        /// <summary>
        /// Принято ли приглашение в группу
        /// </summary>
        public bool isAccepted { get; set; }
    }
}