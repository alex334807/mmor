﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class Account
    {
        public int Id { get; set; }
        /// <summary>
        /// Деньги на данном счету
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// айди группы людей, которые имеют доступ к этому счёту
        /// </summary>
        public int IdGroup { get; set; }
    }
}
