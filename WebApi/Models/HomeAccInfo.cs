﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    /// <summary>
    /// Информация, которая должна присутствовать на главной странице "Аккаунты"
    /// </summary>
    public class HomeAccInfo
    {
        public IEnumerable<NameOfGroup> Groups { get; set; }
        public IEnumerable<Account> Accounts { get; set; } 
    }
}