﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class SignInSignUpJson
    {
        public string login { get; set; }
        public string password { get; set; }
        public bool trySignIn { get; set; }
    }
}