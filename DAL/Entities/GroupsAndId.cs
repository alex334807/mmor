﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class GroupsAndId
    {
        public List<string> groupsOfUser { get; set; }
        public List<int> groupsId { get; set; }
        public List<string> accountsOfUser { get; set; }
    }
}
