﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// Описание сущности пользователя
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        /// <summary>
        /// логин пользователя
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Дата и время, когда пользователь был зарегестрирован
        /// </summary>
        public DateTime TimeOfRegistration { get; set; }
    }
}
