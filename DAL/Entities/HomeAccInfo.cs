﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// Информация, которая должна присутствовать на главной странице "Аккаунты"
    /// </summary>
    public class HomeAccInfo
    {
        public List<string> Groups { get; set; }
        public List<string> Accounts { get; set; }
    }
}
