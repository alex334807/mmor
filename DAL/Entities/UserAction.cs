﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// Этот класс являет собой описание действий происходящих с деньгами
    /// </summary>
    public class UserAction
    {
        /// <summary>
        /// айди операции
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// айди типа операции
        /// </summary>
        public int ActionTypeId { get; set; }
        /// <summary>
        /// Дата проведения операции
        /// </summary>
        public DateTime DateOfOpertion { get; set; }
        /// <summary>
        /// Айди аккаунта с которым была проведена операция
        /// </summary>
        public int AccountId { get; set; }
        /// <summary>
        /// Айди пользователя, который провёл операцию
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Количество денег, задействованных в операции
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// Комментарии пользователя
        /// </summary>
        public string Comment { get; set; }
    }
}
