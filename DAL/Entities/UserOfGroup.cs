﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// Описание сущности пользователя
    /// </summary>
    public class UserOfGroup
    {
        public int Id { get; set; }
        /// <summary>
        /// айди пользователя который состоит в группе
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// айди группы к которой относится пользователь
        /// </summary>
        public int GroupId { get; set; }
    }
}
