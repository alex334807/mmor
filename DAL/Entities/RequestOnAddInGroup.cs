﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class RequestOnAddInGroup
    {
        public int Id { get; set; }
        /// <summary>
        /// айди человека, который приглашает в группу
        /// </summary>
        public int IdUserWhoCreateRequest { get; set; }
        /// <summary>
        /// айди группы в которую приглашают
        /// </summary>
        public int IdGroup { get; set; }
        /// <summary>
        /// айди того, кого приглашают
        /// </summary>
        public int ForWhoThisRequest { get; set; }
    }
}
