﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// Этот класс отвечает за хранение в БД того типа действия, которое было произведено со счётом
    /// </summary>
    public class UserActionType
    {
        public int Id { get; set; }
        /// <summary>
        /// Наименование типа действия
        /// </summary>
        public string TypeOfAction { get; set; }
        /// <summary>
        /// Является ли это пополнением денежных средств
        /// </summary>
        public bool isInvoke { get; set; }
    }
}
