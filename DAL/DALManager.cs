﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.EF;
using DAL.Entities;


namespace DAL
{
    public class DALManager
    {
        private MMOContext db = new MMOContext("DefaultSQL");
        private const string accesDenied = "Test bad return";
        private const string userAdded = "Test return";

        
        /// <summary>
        /// Если метод добавил нового пользователя в базу, то возвращает "Test return" иначе "Test bad return"
        /// </summary>
        /// <param name="forAdding"></param>
        /// <returns></returns>
        public string AddNewUserIfCan(string login, string hashPassword)
        {
            var some = db.Users.ToList<User>();
            //проверяю есть ли пользователь с таким же логином
            var checkedUser = db.Users.FirstOrDefault(t => t.Login == login);

            if (checkedUser == null)
            {
                var lastIndex = db.Users.ToList().Last().Id;
                var newUser = new User { Login = login, TimeOfRegistration = DateTime.Now, Id = lastIndex + 1 };

                db.Users.Add(newUser);
                db.LoginsPasswords.Add(new LoginPassword { Password = hashPassword, UserId = newUser.Id, Id = lastIndex + 1 });
                //db.
                db.SaveChanges();
                lastIndex = -1;
                checkedUser = null;
                return userAdded;
            }
            else
            {
                checkedUser = null;
                return accesDenied;
            }
            
            
        }

        /// <summary>
        /// Проверяет  таблицу пользователей, если находит такой же логин, то сравнивает хэши паролей. возвращает true, если пароль подтверждён
        /// </summary>
        /// <param name="login"></param>
        /// <param name="passwordForCheck"></param>
        /// <returns></returns>
        public bool CheckUserTable(string login, string passwordForCheck)
        {
            //пользователя находим по логину
            var foundedUserLogin = db.Users.FirstOrDefault(t => t.Login == login);
            if (foundedUserLogin == null)
            {
                return false;
            }
            else
            {
                //по айди найденного запрашиваем хэш пароля и сравниваем с предлагаемым хэшем
                var hashPassword = db.LoginsPasswords.FirstOrDefault(t => t.UserId == foundedUserLogin.Id).Password;
                if (hashPassword == passwordForCheck)
                {
                    return true;
                }
                else
                {
                    return true;
                }
            }

        }

        /// <summary>
        /// Возвращает true если запрос на добавление в группу этого пользователя существует
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool CheckGroupRequests(int userId)
        {
            var groupRequest = db.RequestsOnAddInGroups.FirstOrDefault(t => t.ForWhoThisRequest == userId);
            if(groupRequest != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Удаляет группу и пользователей группы
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public string DeleteGroup(int groupId)
        {
            ///Нахожу группу, которую надо удалить
            var founded = db.NamesOfGroups.FirstOrDefault(t => t.Id == groupId);
            ///удаляю группу
            db.NamesOfGroups.Remove(founded);
            db.SaveChanges();
            //Всех пользоваетелй этой группы надо удалять
            var FoundedUserOfGroup = db.UsersOfGroups.FirstOrDefault(t => t.GroupId == groupId);
            while (FoundedUserOfGroup != null)
            {
                ///Пока находится хоть один пользователь удалять его
                db.UsersOfGroups.Remove(FoundedUserOfGroup);
                db.SaveChanges();
                FoundedUserOfGroup = db.UsersOfGroups.FirstOrDefault(t => t.GroupId == groupId);
            }
            db.SaveChanges();

            return " deleted ";
        }

        /// <summary>
        /// Возвращает все счета группы в денежном формате по айди группы
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public decimal[] ReturnAllAccountsOfThisGroup(int groupId)
        {
            ///Нахожу все аккаунты, принадлежашие этой группе
            var accountsOfThisGroup = db.Accounts.Where(t => t.IdGroup == groupId);

            ///деньги конкретного аккаунта?
            var moneyInGroup = new List<decimal>();
            ///Складываю все аккаунты в лист
            foreach (var account in accountsOfThisGroup)
            {
                moneyInGroup.Add(account.Money);
            }
            /// Возвращаю все аккаунты как массив
            return moneyInGroup.ToArray();
        }

        /// <summary>
        ///  Возвращает массив сущностей счетов (айди и деньги счетов)
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public AccountEntity[] GetIdAndMoney(int groupId)
        {
            var accEnt = new List<AccountEntity>();

            ///Нахожу все счета, принадлежашие этой группе
            var accountsOfThisGroup = db.Accounts.Where(t => t.IdGroup == groupId);

            ///Складываю все деньги счетов и айди в лист
            foreach (var account in accountsOfThisGroup)
            {
                accEnt.Add(new AccountEntity { accountId = account.Id, accountMoney = account.Money });
            }
            /// Возвращаю все деньги и айди счётов как массив
            return accEnt.ToArray();
        }

        public string ReturnGroupNameById(int idGroup)
        {
            return db.NamesOfGroups.FirstOrDefault(t => t.Id == idGroup).GroupName;
        }

        /// <summary>
        /// Возвращает имя пользователя по айди
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string ReturnUserNameById(int id)
        {
            return db.Users.FirstOrDefault(t => t.Id == id).Login ;
        }

        /// <summary>
        /// Возвращает сущности приглашений пользователя по айди
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IQueryable<RequestOnAddInGroup>  ReturnUserInvitesById(int userId)
        {
            return db.RequestsOnAddInGroups.Where(t => t.ForWhoThisRequest == userId);
        }

        /// <summary>
        /// Изменяет количество денег на счету в зависимости от типа операции и пришедшего количества денег
        /// </summary>
        /// <param name="moneyAccId"></param>
        /// <param name="money"></param>
        /// <param name="invokeOrNot"></param>
        public void ChangeMoneyOnMoneyAcc(int moneyAccId, decimal money, string invokeOrNot)
        {
            if(invokeOrNot == "+")
            {
                db.Accounts.FirstOrDefault(account => account.Id == moneyAccId).Money += money;
                db.SaveChanges();
            }
            else
            {
                db.Accounts.FirstOrDefault(account => account.Id == moneyAccId).Money -= money;
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Записывает эту операцию в историю
        /// </summary>
        /// <param name="operationTypeId"></param>
        /// <param name="money"></param>
        /// <param name="moneyAccId"></param>
        /// <param name="userId"></param>
        /// <param name="comment"></param>
        public void MakeItHistory(int operationTypeId, decimal money, int moneyAccId, int userId, string comment)
        {
            db.UserActions.Add(new UserAction { ActionTypeId = operationTypeId, DateOfOpertion = DateTime.Now, Money = money, AccountId = moneyAccId, UserId = userId, Comment = comment});
            db.SaveChanges();
        }

        /// <summary>
        /// Возвращает айди операции по её названию
        /// </summary>
        /// <param name="operationTypeName"></param>
        /// <returns></returns>
        public int ReturnOperationIdByOperationName(string operationTypeName)
        {
            return db.UserActionTypes.FirstOrDefault(operatuionTypeEntity => operatuionTypeEntity.TypeOfAction == operationTypeName).Id;
        }

        /// <summary>
        /// Возвращает true, если это операция пополнения счёта
        /// </summary>
        /// <param name="operationType"></param>
        /// <returns></returns>
        public bool IsThatOperationInvoke(string operationType)
        {
            return db.UserActionTypes.FirstOrDefault(actionType => actionType.TypeOfAction == operationType).isInvoke;
        }

        public string[] ReturnAllUsersOfThisGroup(int groupId)
        {
            List<string> NamesArr = new List<string>();
            List<User> UsersOfThisGroup = new List<User>();
            var ElemsOfThisGroup = db.UsersOfGroups.Where(t=> t.GroupId == groupId);
            foreach (var elem in ElemsOfThisGroup)
            {
                UsersOfThisGroup.Add(db.Users.FirstOrDefault(t=> t.Id == elem.UserId));
            }
            foreach (var elem in UsersOfThisGroup)
            {
                NamesArr.Add(elem.Login);
            }

            return NamesArr.ToArray();
        }

        /// <summary>
        /// Возвращает все действия по данному счёту
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public UserAction[] ReturnMoneyAccHistory(int accountId)
        {
            var a = db.UserActions.Where(t => t.AccountId == accountId);
            var c = new UserAction[a.Count()];
            var counter = 0;

            foreach (var elem in a)
            {
                c[counter] = elem;
                counter++;
            }
            
            return c;
        }

        public decimal ReturnMoneyOnMoneyAcc(int intMoneyAccId)
        {
            return db.Accounts.FirstOrDefault(t => t.Id == intMoneyAccId).Money;
        }

        public UserActionType[] ReturnAllActionsType()
        {
            var actionType = db.UserActionTypes.Where(t => t.Id > 0);
            return actionType.ToArray();
        }

        /// <summary>
        /// Проверяет, что пользователь, с предложенным айди находится в группе с таким счётом(возвращает true если есть)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="moneyAccId"></param>
        /// <returns></returns>
        public bool IsThatMoneyAccInGroupsOfThisUser(int userId, int moneyAccId)
        {
            var groupId = db.Accounts.FirstOrDefault(t => t.Id == moneyAccId).IdGroup;
            var foundedGroup = db.UsersOfGroups.FirstOrDefault(user => (user.UserId == userId && user.GroupId == groupId));

            if (foundedGroup != null)
            {
                return true;
            }
            return false;
            
        }

        /// <summary>
        /// После всех проверок переименовывает группу
        /// </summary>
        /// <param name="groupIdForRename"></param>
        /// <param name="newName"></param>
        public void RenameGroup(int groupIdForRename, string newName)
        {
            //var foundForRename = 
            db.NamesOfGroups.FirstOrDefault(t => t.Id == groupIdForRename).GroupName = newName;
            db.SaveChanges();
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupId">по этому id будет найдена группа</param>
        /// <returns></returns>
        public int ReturnGroupOwnerById(int groupId)
        {
            var foundedGroup = db.NamesOfGroups.FirstOrDefault(t => t.Id == groupId);
            return foundedGroup.OwnersId;
        }

        /// <summary>
        /// Создаёт аккаунт
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="idGroup"></param>
        public void AddNewAccount(string userName, int idGroup)
        {
            db.Accounts.Add(new Account() { Money = 0, IdGroup = idGroup });
            db.SaveChanges();
        }

        /// <summary>
        /// Удаляет все операции по счетам, принадлежащим этой группе(Вызывать перед удалением счетов группы)
        /// </summary>
        /// <param name="groupId"></param>
        public void DeleteAllOperationsOfAllAccountsInGroup(int groupId)
        {
            ///нахожу все аккаунты группы
            var allGroupAccounts = db.Accounts.Where(t => t.IdGroup == groupId);
            ///для каждого аккаунта
            foreach(var account in allGroupAccounts)
            {
                ///нахожу все действия по этому аккаунту
                var foundedActions = db.UserActions.Where(t => t.AccountId == account.Id);
                db.SaveChanges();
                ///для каждого найденного действия
                foreach (var action in foundedActions)
                {
                    ///Удаляю найденное действие
                    db.UserActions.Remove(action);
                    db.SaveChanges();
                }
                db.SaveChanges();
            }
            db.SaveChanges();
        }

        /// <summary>
        /// Удаляет все приглашения по айди группы
        /// </summary>
        public void DeleteAllInvitesByGroupId(int groupId)
        {
            var foundedInvites = db.RequestsOnAddInGroups.Where(t => t.IdGroup == groupId);
            foreach(var invite in foundedInvites)
            {
                db.RequestsOnAddInGroups.Remove(invite);
                db.SaveChanges();
            }
            db.SaveChanges();
        }

        /// <summary>
        /// Создаёт группу (Добавляет записи в NameOfGroup и GroupOwner)
        /// </summary>
        /// <param name="nameOfGroup"> название группы, для создания</param>
        /// <param name="ownerId">id пользоваетеля, владеющего группой</param>
        /// <returns></returns>
        public void CreateGroupAndAddOwner(string nameOfGroup, int ownerId)
        {
            db.NamesOfGroups.Add(new NameOfGroup() { OwnersId = ownerId, GroupName = nameOfGroup });
            db.SaveChanges();
            var createdGroupArr = db.NamesOfGroups.Where(t => t.OwnersId == ownerId);
            ///Добавляю пользователя в его старшую по id группу
            db.UsersOfGroups.Add(new UserOfGroup() { GroupId = GetMaxGroupId(createdGroupArr), UserId = ownerId } );
            db.SaveChanges();
            
        }

        public void DeleteOperationsAndAccountsByGroupId(int groupId)
        {
            var foundedAccount = db.Accounts.FirstOrDefault(account => account.IdGroup == groupId);

            while (foundedAccount != null)
            {
                DeleteAllOperaions(foundedAccount);
                db.Accounts.Remove(foundedAccount);
                db.SaveChanges();
                foundedAccount = db.Accounts.FirstOrDefault(account => account.IdGroup == groupId);
            }
            

        }

        private void DeleteAllOperaions(Account foundedAccount)
        {
            var foundedAction = db.UserActions.FirstOrDefault(t => t.AccountId == foundedAccount.Id);
            while (foundedAction != null)
            {
                db.UserActions.Remove(foundedAction);
                db.SaveChanges();
                foundedAction = db.UserActions.FirstOrDefault(t => t.AccountId == foundedAccount.Id);
            }
        }

        /// <summary>
        /// Возвращает большую по Id группу из переданных
        /// </summary>
        /// <param name="entityForSearch"></param>
        /// <returns></returns>
        private int GetMaxGroupId(IQueryable<NameOfGroup> entityForSearch)
        {
            var maxId = 0;
            foreach(var elem in entityForSearch)
            {
                if (elem.Id > maxId)
                {
                    maxId = elem.Id;
                }
            }
            return maxId;
        }

        /// <summary>
        /// Добавляет пользователя в группу
        /// </summary>
        /// <param name="idOfGroup">id группы для добавления пользователя</param>
        /// <param name="idOfUserForAdding"> id пользователя, для добавления в группу</param>
        public void AddUserInGroup(int idOfGroup, int idOfUserForAdding)
        {
            db.UsersOfGroups.Add(new UserOfGroup() { GroupId = idOfGroup, UserId = idOfUserForAdding });
            db.SaveChanges();
        }

        public string AddTokenForUser(string login, string tokenForAdding)
        {
            var lastElem = db.Tokens.ToList().LastOrDefault();

            if (lastElem == null)
            {
                return "не найдено";
            }
            else
            {
                db.Tokens.Add(new Token() { Id = lastElem.Id + 1, UserName = login, TokenName = tokenForAdding, TokenWasAuthorized = DateTime.Now });
                db.SaveChanges();
                return tokenForAdding;
            }
        }

        public void CreateNewAccount(int groupId)
        {
            db.Accounts.Add(new Account() { IdGroup = groupId, Money = 0 });
            db.SaveChanges();
        }


        /// <summary>
        /// Возвращает все Аккаунты, к которым он имееет доступ
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<string> GetAllAccountsById(int userId)
        {
            //записать в лист все id групп к которым пользователь имеет доступ
            var userGroups = db.UsersOfGroups.Where(t => t.UserId == userId);

            /// Id групп, к которым пользователь принадлежит
            List<int> numberOfGroups = new List<int>();

            ///Названия групп, к которым принадлежит пользователь
            var accounts = new List<string>();

            foreach (var a in userGroups)
            {
                numberOfGroups.Add(a.GroupId);
            }

            foreach (var a in numberOfGroups)
            {
                var j = db.Accounts.FirstOrDefault(t => t.IdGroup == a);
                if(j != null)
                {
                    accounts.Add( j.Money.ToString());
                }
                
            }

            return accounts;
        }

        public string ReturnPasswordHashByUserId(int userId)
        {
            var passwordEntity = db.LoginsPasswords.FirstOrDefault(t => t.UserId == userId);
            if (passwordEntity != null)
            {
                return passwordEntity.Password;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Возвращает все группы пользователя, к которым он имеет доступ
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<string> GetAllGroupsById(int userId)
        {
            ///здесь должны лежать Id групп, в которых числится пользователь
            var userGroups = db.UsersOfGroups.Where(t => t.UserId == userId);
            var numberOfGroupsList = new List<int>();
            var groupsOfUserList = new List<string>();
            //test
            var testIdBigger3List = new List<string>();
            var testLIstForGroupsEntity = db.NamesOfGroups;
            foreach(var obj in testLIstForGroupsEntity)
            {
                testIdBigger3List.Add(obj.GroupName);
            }
            //test

            foreach (var a in userGroups)
            {
                numberOfGroupsList.Add(a.GroupId);
            }
            foreach (var userGroupId in numberOfGroupsList)
            {
                //var GroupNameEntity = db.NamesOfGroups.FirstOrDefault(t => t.Id == userGroupId);
                var GroupNameEntity = testLIstForGroupsEntity.FirstOrDefault(t => t.Id == userGroupId);
                if (GroupNameEntity != null )
                {
                    groupsOfUserList.Add(GroupNameEntity.GroupName);
                }
                
            }

            return groupsOfUserList;
        }

        /// <summary>
        /// Возвращает все Id групп в которых состоит пользователь
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<int> GetAllIdOfGroupsById(int userId)
        {
            ///здесь должны лежать группы, в которых числится пользователь
            var userGroups = db.UsersOfGroups.Where(t => t.UserId == userId);
            var groupsId = new List<int>();

            foreach (var a in userGroups)
            {
                groupsId.Add(a.GroupId);
            }

            return groupsId;
        }

        /// <summary>
        /// Добавляет в таблицу приглашений новое приглашение
        /// </summary>
        public void AddInRequestsOnAddInGroups(int idUserWhoCreateRequest, int groupId, int idUserForWhoCreateRequest)
        {
            db.RequestsOnAddInGroups.Add(new RequestOnAddInGroup (){ IdUserWhoCreateRequest = idUserWhoCreateRequest,
                IdGroup = groupId, ForWhoThisRequest = idUserForWhoCreateRequest });
            db.SaveChanges();
        }

        /// <summary>
        /// Удаляет приглашение по айди
        /// </summary>
        /// <param name="inviteId"></param>
        public void DeleteInvite(int inviteId)
        {
            var inviteEntity = db.RequestsOnAddInGroups.FirstOrDefault(t => t.Id == inviteId);
            if(inviteEntity != null)
            db.RequestsOnAddInGroups.Remove(inviteEntity);
            db.SaveChanges();
        }

        /// <summary>
        /// Возвращает айди группы по айди приглашения
        /// </summary>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        public int ReturnGroupIdByInviteId(int inviteId)
        {
            var inviteEntity = db.RequestsOnAddInGroups.FirstOrDefault(t => t.Id == inviteId);
            if (inviteEntity != null)
            {
                return inviteEntity.IdGroup;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Возвращает айди пользователя, по его Логину
        /// </summary>
        /// <param name="invitedUserName"></param>
        /// <returns></returns>
        public int UserIdByName(string invitedUserName)
        {
            var user = db.Users.FirstOrDefault(t => t.Login == invitedUserName);
            if (user != null)
            {
                return user.Id;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Если такое приглашение существует, то возвращает true
        /// </summary>
        /// <param name="invitedId"></param>
        /// <param name="invitedUserId"></param>
        /// <returns></returns>
        public bool IsInviteExist(int inviteId, int invitedUserId)
        {
            var inviteEntity = db.RequestsOnAddInGroups.FirstOrDefault(t => (t.Id == inviteId && t.ForWhoThisRequest == invitedUserId));

            if (inviteEntity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Если существует такой логин - возвращает true
        /// </summary>
        /// <param name="userLoginForAddInGroup"></param>
        /// <returns></returns>
        public bool DoesThisLoginExist(string userLoginForAddInGroup)
        {
            var foundedUser = db.Users.FirstOrDefault(t => t.Login == userLoginForAddInGroup);
            if(foundedUser != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Если такой пользователь есть в этой группе - возвращает true
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public bool CheckIsThisUserInGroup(int userId, int groupId)
        {
            var foundedUser = db.UsersOfGroups.FirstOrDefault(t => t.GroupId == groupId && t.UserId == userId);
            if(foundedUser != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Возвращает + или - в зависимости от того была это операция пополнения или снятия со счёта
        /// </summary>
        /// <param name="actionTypeId"></param>
        /// <returns></returns>
        public string ReturnInvokeOrNot(int actionTypeId)
        {
            if (db.UserActionTypes.FirstOrDefault(operationTypeEntity => operationTypeEntity.Id == actionTypeId).isInvoke)
            {
                return "+";
            }
            return "-";

        }

        public string GetOperationName(int actionTypeId)
        {
            return db.UserActionTypes.FirstOrDefault(type => type.Id == actionTypeId).TypeOfAction;
        }

        /// <summary>
        /// Возвращает id пользователя по его имени (Возвращает -1 если не находит)
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int ReturnIdByUserName(string userName)
        {
            ///присваиваю значение из базы к 
            var valueForCheck = db.Users.FirstOrDefault(t => t.Login == userName);
            if (valueForCheck == null)
            {
                return -1;
            }
            else
            {
                return valueForCheck.Id;
            }
        }

        /// <summary>
        /// Вернуть просто все счета
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        public List<string> GetAccounts(string userLogin)
        {
            var allList = new List<string>();
            //allList.Ad
            return allList;
        }

        /// <summary>
        /// Проверяет есть ли этот пользователь и возвращает хэш пароля и по совместительству токен или null
        /// </summary>
        /// <param name="login"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public string GetToken(string login, string token)
        {
            var foundedUser = db.Users.FirstOrDefault(t => t.Login == login);

            if (foundedUser != null)
            {
                return db.LoginsPasswords.FirstOrDefault(t => t.Id == foundedUser.Id).Password;
            }
            return null;
        }

        /// <summary>
        /// приватный метод, который после всех проверок передаёт инфу обратно
        /// </summary>
        public HomeAccInfo ReturnInfo(string token)
        {
            if (CheckTokenTable(token))
            {
                return null;
            }
            else
            {
                var accInfo = new HomeAccInfo();
                
                return accInfo;
            }
        }

        /// <summary>
        /// Если токен уже существует - возвращает false
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool CheckTokenTable(string token)
        {
            var foundedToken = db.Tokens.FirstOrDefault(t => t.TokenName == token);

            if (foundedToken != null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Возвращает имя пользователя, найденное по токену
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string ReturnUserNameByToken(string token)
        {
            var allTokens = db.Tokens.ToList();
            var foundedByToken = allTokens.Find(t => t.TokenName == token);

            if (foundedByToken == null)
            {
                return "не найдено";
            }
            else
            {
                return foundedByToken.UserName;
            }
        }
    }
}
