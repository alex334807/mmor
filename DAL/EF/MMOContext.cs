﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;


namespace DAL.EF
{
    public class MMOContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<NameOfGroup> NamesOfGroups { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<LoginPassword> LoginsPasswords { get; set; }
        public DbSet<UserAction> UserActions { get; set; }
        public DbSet<UserActionType> UserActionTypes { get; set; }
        public DbSet<UserOfGroup> UsersOfGroups { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<RequestOnAddInGroup> RequestsOnAddInGroups { get; set; }

        static MMOContext()
        {
            Database.SetInitializer<MMOContext>(new StoreDbInitializer());
        }
        public MMOContext(string connectionString) : base(connectionString)
        {
            //Database.Initialize(false);
            
        }
    }

    public class StoreDbInitializer : DropCreateDatabaseAlways<MMOContext>
    {
        
        protected override void Seed(MMOContext db)
        {
            db.Users.Add(new User() { Id = 1, Login = "alexei23", TimeOfRegistration = DateTime.Now });
            db.Users.Add(new User() { Id = 2, Login = "Natash43", TimeOfRegistration = DateTime.Now });
            db.Users.Add(new User() { Id = 3, Login = "Dimitr", TimeOfRegistration = DateTime.Now });
            db.Users.Add(new User() { Id = 4, Login = "alexei", TimeOfRegistration = DateTime.Now });
            db.Users.Add(new User() { Id = 5, Login = "Andrei", TimeOfRegistration = DateTime.Now });

            db.LoginsPasswords.Add(new LoginPassword() { Id = 1, UserId = 1, Password = "-888439125" });
            db.LoginsPasswords.Add(new LoginPassword() { Id = 2, UserId = 2, Password = "-500370718" });
            db.LoginsPasswords.Add(new LoginPassword() { Id = 3, UserId = 3, Password = "-112302179" });
            db.LoginsPasswords.Add(new LoginPassword() { Id = 4, UserId = 4, Password = "275766236" });

            db.UserActionTypes.Add(new UserActionType() { Id = 1, TypeOfAction = "Зарплата", isInvoke = true });
            db.UserActionTypes.Add(new UserActionType() { Id = 2, TypeOfAction = "Проценты с вклада", isInvoke = true });
            db.UserActionTypes.Add(new UserActionType() { Id = 3, TypeOfAction = "Выигрыш", isInvoke = true });
            db.UserActionTypes.Add(new UserActionType() { Id = 4, TypeOfAction = "Найдено", isInvoke = true });
            db.UserActionTypes.Add(new UserActionType() { Id = 5, TypeOfAction = "Персональный транспорт", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 6, TypeOfAction = "Публичный транспорт", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 7, TypeOfAction = "Проиграл", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 8, TypeOfAction = "Потерял", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 9, TypeOfAction = "На жильё", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 10, TypeOfAction = "Питание", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 11, TypeOfAction = "Здоровье", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 12, TypeOfAction = "Животные", isInvoke = false });
            db.UserActionTypes.Add(new UserActionType() { Id = 13, TypeOfAction = "Налоги", isInvoke = false });

            db.UserActions.Add(new UserAction() { Id = 1, ActionTypeId = 1, Money = 1000, DateOfOpertion = DateTime.Now, AccountId = 1, UserId = 1, Comment = "зарплатка моя" });
            db.UserActions.Add(new UserAction() { Id = 2, ActionTypeId = 1, Money = 50300, DateOfOpertion = DateTime.Now, AccountId = 2, UserId = 3, Comment = ""});
            db.UserActions.Add(new UserAction() { Id = 3, ActionTypeId = 1, Money = 22230, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 4, Comment = "Зарплату получил" });
            db.UserActions.Add(new UserAction() { Id = 4, ActionTypeId = 1, Money = 53450, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 3, Comment = "Получил зарплату" });
            db.UserActions.Add(new UserAction() { Id = 5, ActionTypeId = 12, Money = 450, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 1, Comment = "Купил еды собаке" });
            db.UserActions.Add(new UserAction() { Id = 6, ActionTypeId = 10, Money = 3000, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 3, Comment = "" });
            db.UserActions.Add(new UserAction() { Id = 7, ActionTypeId = 7, Money = 20000, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 2, Comment = "" });
            db.UserActions.Add(new UserAction() { Id = 8, ActionTypeId = 11, Money = 230, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 3, Comment = "" });
            db.UserActions.Add(new UserAction() { Id = 9, ActionTypeId = 8, Money = 3000, DateOfOpertion = DateTime.Now, AccountId = 3, UserId = 4, Comment = "" });

            db.NamesOfGroups.Add(new NameOfGroup() { Id = 1, OwnersId = 1, GroupName = "family" });
            db.NamesOfGroups.Add(new NameOfGroup() { Id = 2, OwnersId = 3, GroupName = "family" });
            db.NamesOfGroups.Add(new NameOfGroup() { Id = 3, OwnersId = 3, GroupName = "holidays" });

            db.Accounts.Add(new Account() { Id = 1, Money = 10000, IdGroup = 1 });
            db.Accounts.Add(new Account() { Id = 2, Money = 11000, IdGroup = 2 });
            db.Accounts.Add(new Account() { Id = 3, Money = 8000, IdGroup = 3 });

            db.UsersOfGroups.Add(new UserOfGroup() { Id = 1, UserId = 1, GroupId = 1 });
            db.UsersOfGroups.Add(new UserOfGroup() { Id = 2, UserId = 2, GroupId = 1 });
            db.UsersOfGroups.Add(new UserOfGroup() { Id = 3, UserId = 2, GroupId = 2 });
            db.UsersOfGroups.Add(new UserOfGroup() { Id = 4, UserId = 3, GroupId = 2 });
            db.UsersOfGroups.Add(new UserOfGroup() { Id = 5, UserId = 3, GroupId = 3 });
            db.UsersOfGroups.Add(new UserOfGroup() { Id = 6, UserId = 4, GroupId = 3 });
            db.UsersOfGroups.Add(new UserOfGroup() { Id = 7, UserId = 4, GroupId = 2 });

            db.Tokens.Add(new Token() { Id = 1, TokenName = "password123", TokenWasAuthorized = DateTime.Now, UserName = "testLogin"});

            db.RequestsOnAddInGroups.Add(new RequestOnAddInGroup() { IdUserWhoCreateRequest = 1, IdGroup = 1,  ForWhoThisRequest = 4});

            db.SaveChanges();
        }
    }
}