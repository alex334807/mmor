import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class MyHttpService 
{
  //private asd: AccountsComponent;
  private ConnectionError: string = 'СОЕДИНЕНИЕ ОТКЛОНЕНО      ';
  //public myResponse : HomeAccInfo;
  

  constructor(private http: HttpClient, private router: Router) 
  {

  }

  //Метод выхода и регистрации в аккаунт
  public postSignInSignUpInform(login: string, password: string, trySignIn: boolean)
  {
    console.log('service postSignInSignUpInform work!');
    const body = {login: login, password: password, trySignIn: trySignIn};

    return this.http.post('http://localhost:50299/api/mmo/signInSignUp', body).subscribe(value =>
    {
      let myResponse = value.toString();
      console.log( 'Сервер ответил на запрос регистрации или входа ' + myResponse);

      if(body.trySignIn)
      {
        if(myResponse == 'Отказано' || myResponse == '4')
        {
          if(myResponse == 'Отказано'){alert('Логин или пароль не верны');}
          if(myResponse == '4'){alert('Логин или пароль слишком короткий');}
        }
        else
        {
          if(localStorage.getItem('doSessionStorage') == 'session'){
            sessionStorage.setItem('MyToken', value.toString());
            sessionStorage.setItem('UserName',login );
            console.log('записал в локальное хранилище "MyToken" ' + sessionStorage.getItem('MyToken'));
          }
          else{
            localStorage.setItem('MyToken', value.toString());
            localStorage.setItem('UserName',login );
            console.log('записал в локальное хранилище "MyToken" ' + localStorage.getItem('MyToken'));
          }
          
          this.router.navigate(['accounts']);
        }
      }
      else
      {
        if(myResponse == 'Test return')
        {
          alert('Вы были успешно зарегестрированы');
        }
        else
        {
          alert('Попытка регистрации провалилась :с');
        }
      }
      
    },
    error => 
    {
      alert(this.ConnectionError + error.message);
      
    });
  }

  public testGetJson(token: string){
    console.log('service testGetJson work!');
    //const body = {token: "token123"};
    //let response;
        
    return this.http.post('http://localhost:50299/api/mmo/testGetJson', 'qwe123qwe').subscribe(value => {
      
      console.log(value.toString());
     
    },
    error => {
      alert(this.ConnectionError + error.message);
      
    });
  }

  // public testUpdatedJson(token : string){
  //   console.log('service testUpdatedJson work!');
  //   let testClass = {abv : token};
        
  //   return this.http.post('http://localhost:50299/api/mmo/updatedTest', testClass).subscribe(value => {
      
  //     //this.myResponse = value as HomeAccInfo;
  //     console.log('получил на запрос инф с домашней страницы ' + value.toString());    
      
  //   },
  //   error => {
  //     alert(this.ConnectionError + error.message);
      
  //   });
  // }

}

// export class HomeAccInfo {
//   public Groups = new Array;
//   public Accounts= new Array;
// }