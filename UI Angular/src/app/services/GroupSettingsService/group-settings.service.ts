import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GroupSettingsService {
  public myResponse : EntityAllUsersOfGroup;
  
  constructor(private http : HttpClient, private router: Router) { }

  public getAllUsersOfThisGroup(groupId: number) {
    let token: string;
    console.log('method getAllUsers work!' + 'groupId =' + groupId);

    if(localStorage.getItem('doSessionStorage') == 'session'){
      token = sessionStorage.getItem('MyToken');
    }
    else{
      token = localStorage.getItem('MyToken');
    }

    let allUsersOfGroupRequest = {Token : token, GroupId : groupId};
        
    return this.http.post('http://localhost:50299/api/mmo/GetAllUsersOfGroup', allUsersOfGroupRequest).subscribe(value => {
      console.log('получил ответ о добавлении ' + value.toString());
      this.myResponse = value as EntityAllUsersOfGroup;
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });
    
  }
  
  public addNewAccount(groupId: number) {
    let token: string;
    console.log('method addAccount work!' + 'groupId =' + groupId);

    if(localStorage.getItem('doSessionStorage') == 'session'){
      token = sessionStorage.getItem('MyToken');
    }
    else{
      token = localStorage.getItem('MyToken');
    }

    let renameGroupReq = {Token : token, GroupId : groupId};
        
    return this.http.post('http://localhost:50299/api/mmo/AddAccount', renameGroupReq).subscribe(value => {
      console.log('получил ответ о добавлении ' + value.toString());
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });
  }

  public renameGroup(groupId: number, newName: string) {
    let token: string;
    console.log('method renameGroup work!' + 'groupId =' + groupId);

    if(localStorage.getItem('doSessionStorage') == 'session'){
      token = sessionStorage.getItem('MyToken');
    }
    else{
      token = localStorage.getItem('MyToken');
    }

    let renameGroupReq = {Token : token, GroupId : groupId, newName};
        
    return this.http.post('http://localhost:50299/api/mmo/RenameGroup', renameGroupReq).subscribe(value => {
      console.log('получил ответ о переименовании ' + value.toString());
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });
  }

  public deleteGroup(groupId: number){
    let token: string;
    console.log('method deleteGroup work!' + 'groupId =' + groupId);

    if(localStorage.getItem('doSessionStorage') == 'session'){
      token = sessionStorage.getItem('MyToken');
    }
    else{
      token = localStorage.getItem('MyToken');
    }

    let deleteGroupReq = {Token : token, GroupId : groupId};
        
    return this.http.post('http://localhost:50299/api/mmo/DeleteGroup', deleteGroupReq).subscribe(value => {
      console.log('получил ответ о удалении ' + value.toString());
      this.router.navigate(['accounts']);
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });
  }

  public accountExit(){

  }

  public addRequestOnAddingUserInGroup(groupId:number, userLogin: string){
    let token: string;
    console.log('method addAccount work!' + 'userLogin =' + userLogin);

    if(localStorage.getItem('doSessionStorage') == 'session'){
      token = sessionStorage.getItem('MyToken');
    }
    else{
      token = localStorage.getItem('MyToken');
    }

    let addUserReq = {Token : token, GroupId : groupId, NewName : userLogin};

    return this.http.post('http://localhost:50299/api/mmo/AddRequestOnAddingUserInGroup', addUserReq).subscribe(value => {
      console.log('получил ответ о добавлении запроса о добавлении ' + value.toString());
      alert(value.toString());
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });

  }

  public routRegistrationLogIn(){
    this.router.navigate(['qwe']); 
  }
}

export class EntityAllUsersOfGroup {
  public AllUsersOfGroup = new Array();
  public AllAccountsOfGroup = new Array<AccountEntity>();
}

export class AccountEntity{
  public accountId: number;
  public accountMoney: number;
}