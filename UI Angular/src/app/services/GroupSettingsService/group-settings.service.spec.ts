import { TestBed } from '@angular/core/testing';

import { GroupSettingsService } from './group-settings.service';

describe('GroupSettingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupSettingsService = TestBed.get(GroupSettingsService);
    expect(service).toBeTruthy();
  });
});
