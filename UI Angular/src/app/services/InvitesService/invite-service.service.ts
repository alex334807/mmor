import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InviteServiceService {
  
  public myResponse:  InviteElements;

  constructor(private http: HttpClient) {

  }

  public GetAllUsersInvites(token: string){

    console.log('service Invites work!');
    let InviteReq = {token : token};
        
    return this.http.post('http://localhost:50299/api/mmo/GetAllUserInvites', InviteReq).subscribe(value => {
      
      this.myResponse = value as InviteElements;
      console.log('' );
         
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });


  }

  public ReturnResponseOnAnvite(token: string, inviteId: number, isAccepted: boolean){

    console.log('service Invites work!');
    let InviteResponse = {token : token, InviteId: inviteId, isAccepted: isAccepted};
        
    return this.http.post('http://localhost:50299/api/mmo/ResponseOnInvite', InviteResponse).subscribe(value => {
      
      this.myResponse = value as InviteElements;
      console.log('' );
         
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });

  }



}

export class InviteElements{
  public InviteElements = Array<InviteElement>();
}

export class InviteElement{
  public nameOfGroup: string;
  public loginOfUserWhoCreateReq: string;
  public inviteId: number;
}
