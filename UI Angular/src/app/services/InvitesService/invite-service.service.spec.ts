import { TestBed } from '@angular/core/testing';

import { InviteServiceService } from './invite-service.service';

describe('InviteServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InviteServiceService = TestBed.get(InviteServiceService);
    expect(service).toBeTruthy();
  });
});
