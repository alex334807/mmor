import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MoneyAccountService {
  public myResponse: HistoryAndType;

  constructor(private http: HttpClient) { }

  public getHisoryAndTypes(token: string, moneyAccId: string){
    console.log('service moneyAccountSrvice work');
    const body = {token: token, moneyAccId: moneyAccId};
    
        
    return this.http.post('http://localhost:50299/api/mmo/GetHistoryAnfTypes', body).subscribe(value => {
      
      console.log(value.toString());
      this.myResponse = value as HistoryAndType;

    },
    error => {
      alert('ОШИБКА:  ' + error.message);
      
    });
  }

  public doOperation(token: string, moneyAccountId: string, operationType: string, money: number, comment: string){
    
    console.log('service moneyAccountSrvice work');
    const body = {token: token, moneyAccId: parseInt(moneyAccountId), operationType: operationType, money: money, comment: comment};
    
        
    return this.http.post('http://localhost:50299/api/mmo/DoOperation', body).subscribe(value => {
      
      console.log(value.toString());
      
    },
    error => {
      alert('ОШИБКА:  ' + error.message);
      
    });
  }


}

export class HistoryAndType {
  public money: number;
  public ActionHistory = Array<ActionEntity>();
  public TypesOfAction = new TypesOfActionEntity();
  
  
  /*
  decimal money;
  ActionEntity[] actionHistory;
  string[] typesOfAction;
  */
}

export class ActionEntity{
  public Money: number;
  public TimeOperation: string;
  public UserName: string;
  public Comment: string;
  public OperationType: string;
  public IsInvoke: string;
  
  /*
    decimal money;
    string timeOperation;
    string userName;
    string comment;
    string operationType;
    string isInvoke;
  */
}

export class TypesOfActionEntity{
  public Invoke = Array<string>();
  public NotInvoke = Array<string>();

  /*
  string[] invoke;
  string[] notInvoke;
  */
}
