import { TestBed } from '@angular/core/testing';

import { MoneyAccountService } from './money-account.service';

describe('MoneyAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MoneyAccountService = TestBed.get(MoneyAccountService);
    expect(service).toBeTruthy();
  });
});
