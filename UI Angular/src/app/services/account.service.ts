import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import { GroupSettingsComponent } from 'src/app/components/group-settings/group-settings.component'
import { MessagesComponent} from 'src/app/components/messages/messages/messages.component'

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public myResponse : HomeAccInfo;
  //public testClass : MyNewTestClass;

  constructor(private http: HttpClient, private router: Router, /*private messageComponent: MessagesComponent*/ /*grouSett : GroupSettingsComponent*/) { }

  public getAccInfo(token : string)
  {
    console.log('service testUpdatedJson work!');
    let AccInfRequest = {token : token};
        
    return this.http.post('http://localhost:50299/api/mmo/GetAccInfo', AccInfRequest).subscribe(value => {
      
      this.myResponse = value as HomeAccInfo;
      console.log('пишу в sessionStorage, о сообщениях :' + this.myResponse.IsGroupRequestExist.toString() );
      sessionStorage.setItem('newMessage', this.myResponse.IsGroupRequestExist.toString());
      //MessagesComponent.newMessage = this.myResponse.IsGroupRequestExist;
      //this.messageComponent.newMessage = this.myResponse.IsGroupRequestExist;
      //this.testClass.HomeInfo = this.myResponse;
      
      console.log('получил на запрос инф с домашней страницы ' + this.myResponse.Accounts.toString());
      
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });
  }

  public makeNewGroup(token: string, nameForGroup: string){
    console.log('method makeNewGroupAccount work!');
    let newGroupReq = {token : token, nameForGroup : nameForGroup};
        
    return this.http.post('http://localhost:50299/api/mmo/CreateGroup', newGroupReq).subscribe(value => {
      console.log('получил ответ о создании ' + value.toString());
      
    },
    error => {
      alert('ОШИБКА ' + error.message);
      
    });
  }

  public getAllAboutGroup(groupId: number) {
    //GroupSettingsComponent.Id = groupId;
    this.router.navigate(['accounts/groupSettings']);
  }

  public routRegistrationLogIn(){
    this.router.navigate(['qwe']);
  }
}

export class HomeAccInfo {
  public Accounts = new Array;
  public ListGroupArrElem = Array<GroupsArrElem>() ;
  public IsGroupRequestExist:boolean;
}

export class GroupsArrElem {
  public id: number;
  public group: string;
}
