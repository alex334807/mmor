import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './components/registration/registration.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { NotifierModule } from 'angular-notifier';
import { GroupSettingsComponent } from './components/group-settings/group-settings.component';
import { MessagesComponent } from './components/messages/messages/messages.component';
import { InvitesComponent } from './components/invites/invites.component';
import { MoneyActionsComponent } from './components/money-actions/money-actions.component';
import { MoneyAccountComponent } from './components/money-account/money-account.component';

const appRoutes: Routes =[
  {path: '', component: RegistrationComponent},
  {path: 'accounts', component: AccountsComponent},
  {path: 'accounts/groupSettings', component: GroupSettingsComponent},
  {path: 'accounts/invites', component: InvitesComponent},
  {path: 'accounts/groupSettings/money_account' , component: MoneyAccountComponent},
  {path: '**', redirectTo: ''} 
];

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    AccountsComponent,
    GroupSettingsComponent,
    MessagesComponent,
    InvitesComponent,
    MoneyActionsComponent,
    MoneyAccountComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NotifierModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
