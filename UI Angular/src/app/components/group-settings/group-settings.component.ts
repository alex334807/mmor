import { Component, OnInit } from '@angular/core';
import { GroupSettingsService } from 'src/app/services/GroupSettingsService/group-settings.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-group-settings',
  templateUrl: './group-settings.component.html',
  styleUrls: ['./group-settings.component.css']
})
export class GroupSettingsComponent implements OnInit {

  GroupName: string = sessionStorage.getItem('IdGroup') + ': ' + sessionStorage.getItem('NameGroup');

  constructor(private groupSettServ : GroupSettingsService, private router: Router) { }

  
  ngOnInit(): void {
    this.checkTheme();
    this.getGroupActions();
    this.getGroupUsers();
  }

  private getGroupActions() {
    
  }

  private getGroupUsers(){
    let groupIdForGettingUsers = +sessionStorage.getItem('IdGroup');
    console.log('Запрашиваю всех пользователей группы');
    this.groupSettServ.getAllUsersOfThisGroup(groupIdForGettingUsers);
  }

  routOnMoneyAccount(moneyAccountId: number){
    console.log(moneyAccountId);
    sessionStorage.setItem('MoneyGroupId', moneyAccountId.toString());
    this.router.navigate(['accounts/groupSettings/money_account']);
  }

  public deleteGroup(){
    let groupForDelete = +sessionStorage.getItem('IdGroup');
    console.log("Удаляю группу :" + groupForDelete);
    this.groupSettServ.deleteGroup(groupForDelete);
  }

  public renameGroup(newName: string){
    let groupForRename = +sessionStorage.getItem('IdGroup');
    console.log("Переименую группу : " + groupForRename);
    this.groupSettServ.renameGroup(groupForRename, newName);
    this.GroupName = sessionStorage.getItem('IdGroup') + ': ' + newName;
  }

  public addNewAccount(){
    let groupForAddAccount = +sessionStorage.getItem('IdGroup');
    console.log("Добавляю аккаунт к группе: " + groupForAddAccount);
    this.groupSettServ.addNewAccount(groupForAddAccount);
    location.reload();
  }

  public addRequestOnAddingUserInGroup(userLogin: string){
    console.log('из groupComponent пишу: ' + userLogin);
    let groupForNewUser = +sessionStorage.getItem('IdGroup');
    console.log("Добавляю пользователя к группе: " + groupForNewUser)
    this.groupSettServ.addRequestOnAddingUserInGroup(groupForNewUser,userLogin);
  }

  public addTypeOfOperation(){

  }

  public accountExit(){
    sessionStorage.clear();
    sessionStorage.setItem('theme',localStorage.getItem('theme'));
    localStorage.clear();
    localStorage.setItem('theme',sessionStorage.getItem('theme'));    
    this.groupSettServ.routRegistrationLogIn();
  }

  public getGroupInfo(token: string, groupId: number){

  }

  private makeThemeLight(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#aaa' ;
      document.body.style.backgroundImage = 'url(images/DayTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#ffc700';
  }

  private makeThemeDark(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#000' ;
      document.body.style.backgroundImage = 'url(images/NightTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#b79634';
  }

  private checkTheme(){
    if(localStorage.getItem("theme") == 'white'){
      this.makeThemeLight();
    }
    else{
      this.makeThemeDark();
    }
  }

  public changeLocalStorageTheme(): void {
    if(localStorage.getItem('theme') == 'white')
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'black');
      this.checkTheme();
    }
    else
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'white');
      this.checkTheme();
    }

  }

}
