import { Component, OnInit} from '@angular/core';
import { MyHttpService } from 'src/app/services/HttpService/my-http.service';
import { Router } from '@angular/router';
import { MessagesComponent } from 'src/app/components/messages/messages/messages.component'
import { from } from 'rxjs';

//import { AccountsComponent } from 'src/app/components/accounts/accounts.component'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  public myLocalToken:string = '1';
  public messComponent: MessagesComponent;

  constructor(private MyHttp: MyHttpService, private router: Router) {
    this.checkTheme();
    this.tryLogInLocalUser();
  }

  ngOnInit() {
    
    //Вывод количества пользователей???
    //Количество проведённых операций???
    //this.publicInfo = this.MyHttp.getPublicInformation;
    
  }

  private checkTheCheckbox(){
    let myCheckBox = document.getElementById('myCheckbox') as HTMLInputElement;
    if (myCheckBox != null) {
      if(myCheckBox.checked)
      {
        console.log('do sessionStorage');
        localStorage.setItem('doSessionStorage', 'session');
      }
      else
      {
        console.log('do localStorage');
        localStorage.setItem('doSessionStorage', '-1');
      }
    }
    else{
      console.log("Не нашёл чекбокса");
    }
   
  }

  public tryLogInLocalUser(){
    this.myLocalToken = localStorage.getItem('MyToken');

    console.log('local token: ' + this.myLocalToken);
     if(localStorage.getItem('MyToken') != null)
     {
      this.routOnAccount();
     }
     
  }

  public postSingInSignUp(login: string, password: string, trySignIn: boolean){
    console.log('postSingInSignUp');
    this.checkTheCheckbox();
    this.MyHttp.postSignInSignUpInform(login, password, trySignIn);
  }

  public routOnAccount(){
    this.router.navigate(['accounts']);
  }

  public testGetJson(){
    console.log('testGetJson');
    console.log('запустил testUpdatedJson');
    
  }

  private checkTheme(){
    if(localStorage.getItem("theme") == 'white'){
      this.makeThemeLight();
    }
    else{
      this.makeThemeDark();
    }
  }

  private makeThemeLight(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#aaa' ;
      document.body.style.backgroundImage = 'url(images/registration.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      //document.body.style.color = '#ffc700';
  }

  private makeThemeDark(){
    console.log('Вроде как меняю тему');
    document.body.style.backgroundColor = '#000' ;
    document.body.style.backgroundImage = 'url(images/registrationNight.gif)';
    document.body.style.backgroundRepeat = 'no-repeat';
    document.body.style.backgroundSize = '100%';
    //document.body.style.color = '#aaa';
    let shand = document.getElementsByClassName('.myForm') as HTMLCollectionOf<HTMLElement>;
    if (shand.length != 0) {
      shand[0].style.color = "#000";
      shand[0].style.backgroundImage = 'radial-gradient(#f6d465d8, #fd9f85d8, #f6d465d8)';
    }
    let shand1 = document.getElementsByClassName('.talbePubInf') as HTMLCollectionOf<HTMLElement>;
    if (shand1.length != 0) {
      shand1[0].style.color = "#000";
      shand1[0].style.backgroundImage = 'radial-gradient(#fff, #000, #aaa)';
    }
  }

}