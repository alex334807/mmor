import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyActionsComponent } from './money-actions.component';

describe('MoneyActionsComponent', () => {
  let component: MoneyActionsComponent;
  let fixture: ComponentFixture<MoneyActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
