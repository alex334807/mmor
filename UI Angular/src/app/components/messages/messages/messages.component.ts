import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { AccountService } from 'src/app/services/account.service';
//import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  //public static newMessage: boolean = true;
  constructor(private router: Router, private accServ: AccountService) {
    
  }

  ngOnInit() {
    
  }

  checkMessagesOnTime(){
    
  }

  public navigateOnInvitesComponent(){
    this.router.navigate(['accounts/invites']);
  }

  // public switchIt(){
  //   this.newMessage = !this.newMessage;
  // }

  // private checkMessage(){
  //   if(isNullOrUndefined(this.accServ.myResponse)){
  //     this.newMessage = false;
  //     console.log('messageComponent не нашёл Response');
  //   }
  //   else{
  //     this.newMessage = this.accServ.myResponse.IsGroupRequestExist;
  //   }
  // }

}
