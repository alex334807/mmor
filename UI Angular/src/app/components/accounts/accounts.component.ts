import { Component, OnInit, LOCALE_ID} from '@angular/core';
import { MyHttpService } from 'src/app/services/HttpService/my-http.service';
import { AccountService } from 'src/app/services/account.service';
import { GroupSettingsComponent } from '../group-settings/group-settings.component';
import { Router } from '@angular/router'


@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  public myLocalToken:string = '1';
  public homeInfo: HomeAccInfo;
  public userName: string = '';
  public _accounts = ['wewe'];
  items =['Tom', 'Bob', 'Sam', 'Bill'];
  
  constructor(private AccHttpServ: AccountService, private router: Router) {
    this.tryLogInLocalUser();
    this.getUserInformation();
    this.checkTheme();
  }

  public getUserInformation(){
    if(localStorage.getItem('doSessionStorage') == 'session'){
      this.AccHttpServ.getAccInfo(sessionStorage.getItem('MyToken'));
    }
    else{
      this.AccHttpServ.getAccInfo(localStorage.getItem('MyToken'));
    }
  }

  public changeLocalStorageTheme(): void {
    if(localStorage.getItem('theme') == 'white')
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'black');
      this.checkTheme();
    }
    else
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'white');
      this.checkTheme();
    }

  }

  ngOnInit() {
    console.log( 'accounts begun');
    
  }

  public tryLogInLocalUser(){
    this.myLocalToken = localStorage.getItem('MyToken');

    console.log('local token: ' + this.myLocalToken);
     if(localStorage.getItem('MyToken') == null && sessionStorage.getItem('MyToken') == null)
     {
      console.log('перенаправляю на страницу входа');
      this.router.navigate(['registration']);
     }
     else
     {
       //Установить логин пользователя
      if(localStorage.getItem('MyToken') == null)
      {
        this.userName = sessionStorage.getItem('UserName');
      }
      else
      {
        this.userName = localStorage.getItem('UserName');
      }
     }     
  }

  private checkTheme(){
    if(localStorage.getItem("theme") == 'white'){
      this.makeThemeLight();
    }
    else{
      this.makeThemeDark();
    }
  }

  private makeThemeLight(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#aaa' ;
      document.body.style.backgroundImage = 'url(images/DayTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#ffc700';
  }

  private makeThemeDark(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#000' ;
      document.body.style.backgroundImage = 'url(images/NightTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#b79634';
  }

  public makeNewGroup(name: string){
    if(localStorage.getItem('doSessionStorage') == 'session'){
      this.AccHttpServ.makeNewGroup(sessionStorage.getItem('MyToken'), name);
    }
    else{
      this.AccHttpServ.makeNewGroup(localStorage.getItem('MyToken'), name);
      
    }
    location.reload();
  }

  public RoutOnGroupSettings(IdOfGroup: number, NameOfGroup: string){
    console.log("Отправляю на настройку группы " + IdOfGroup);
    sessionStorage.setItem('IdGroup', IdOfGroup.toString());
    sessionStorage.setItem('NameGroup', NameOfGroup.toString());
    this.router.navigate(['accounts/groupSettings']);
  }

  public accountExit(){
    sessionStorage.clear();
    sessionStorage.setItem('theme',localStorage.getItem('theme'));
    localStorage.clear();
    localStorage.setItem('theme',sessionStorage.getItem('theme'));    
    this.AccHttpServ.routRegistrationLogIn();
  }

}

export class HomeAccInfo {
  public Accounts = new Array;
  public ListGroupArrElem = Array<GroupsArrElem>() ;
  public IsGroupRequestExist: boolean = false;
}

export class GroupsArrElem {
  public group: string;
  public id: number;
}