import { Component, OnInit } from '@angular/core';
import { InviteServiceService } from 'src/app/services/InvitesService/invite-service.service'

@Component({
  selector: 'app-invites',
  templateUrl: './invites.component.html',
  styleUrls: ['./invites.component.css']
})
export class InvitesComponent implements OnInit {

  private userName: string;

  constructor(private inviteServ: InviteServiceService) {
    this.ShowUserName();
    this.getUserInvites();
    this.checkTheme();
   }

  ngOnInit() {
  }

  public SetAnswer(InviteId: number, IsAccepted: boolean){
    console.log(InviteId + '  ' +  IsAccepted);
    let token = localStorage.getItem("MyToken");
    if(token == '' || token == null){token = sessionStorage.getItem("MyToken")} 
    this.inviteServ.ReturnResponseOnAnvite(token, InviteId, IsAccepted);
    location.reload();
  }

  private ShowUserName(){
    //Установить логин пользователя
    if(localStorage.getItem('MyToken') == null)
    {
      this.userName = sessionStorage.getItem('UserName');
    }
    else
    {
      this.userName = localStorage.getItem('UserName');
    }
  }

  public getUserInvites(){
    if(localStorage.getItem('doSessionStorage') == 'session'){
      this.inviteServ.GetAllUsersInvites(sessionStorage.getItem('MyToken'));
    }
    else{
      this.inviteServ.GetAllUsersInvites(localStorage.getItem('MyToken'));
    }
  }

  public changeLocalStorageTheme(): void {
    if(localStorage.getItem('theme') == 'white')
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'black');
      this.checkTheme();
    }
    else
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'white');
      this.checkTheme();
    }

  }

  private checkTheme(){
    if(localStorage.getItem("theme") == 'white'){
      this.makeThemeLight();
    }
    else{
      this.makeThemeDark();
    }
  }

  private makeThemeLight(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#aaa' ;
      document.body.style.backgroundImage = 'url(images/DayTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#ffc700';
  }

  private makeThemeDark(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#000' ;
      document.body.style.backgroundImage = 'url(images/NightTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#b79634';
  }

}
