import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyAccountComponent } from './money-account.component';

describe('MoneyAccountComponent', () => {
  let component: MoneyAccountComponent;
  let fixture: ComponentFixture<MoneyAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
