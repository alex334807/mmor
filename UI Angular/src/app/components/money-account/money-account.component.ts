import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import {MoneyAccountService} from 'src/app/services/moneyAccount/money-account.service'

@Component({
  selector: 'app-money-account',
  templateUrl: './money-account.component.html',
  styleUrls: ['./money-account.component.css']
})
export class MoneyAccountComponent implements OnInit {

  public invokeClicked = 0;
  public typeForAction = '';

  constructor(private router: Router, private moneyAccServ: MoneyAccountService) { }

  ngOnInit() {
    this.checkTheme();
    this.getTypesAndActionHistory();
  }

  routOnMoneyAccount(){
    this.router.navigate(['accounts/groupSettings/money_account']);
  }

  getTypesAndActionHistory(){

    if(localStorage.getItem('doSessionStorage') == 'session'){
      this.moneyAccServ.getHisoryAndTypes(sessionStorage.getItem('MyToken'), sessionStorage.getItem('MoneyGroupId'));
    }
    else{
      this.moneyAccServ.getHisoryAndTypes(localStorage.getItem('MyToken'), sessionStorage.getItem('MoneyGroupId'));
    }
  }

  private checkTheme(){
    if(localStorage.getItem("theme") == 'white'){
      this.makeThemeLight();
    }
    else{
      this.makeThemeDark();
    }
  }

  private makeThemeLight(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#aaa' ;
      document.body.style.backgroundImage = 'url(images/DayTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#ffc700';
  }

  private makeThemeDark(){
    console.log('Вроде как меняю тему');
      document.body.style.backgroundColor = '#000' ;
      document.body.style.backgroundImage = 'url(images/NightTheme.gif)';
      document.body.style.backgroundRepeat = 'no-repeat';
      document.body.style.backgroundSize = '100%';
      document.body.style.color = '#b79634';
  }

  public changeLocalStorageTheme(): void {
    if(localStorage.getItem('theme') == 'white')
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'black');
      this.checkTheme();
    }
    else
    {
      console.log('Вроде как меняю тему');
      localStorage.setItem('theme', 'white');
      this.checkTheme();
    }

  }

  public accountExit(){
    console.log('Выхожу из аккаунта');
    sessionStorage.clear();
    sessionStorage.setItem('theme',localStorage.getItem('theme'));
    localStorage.clear();
    localStorage.setItem('theme',sessionStorage.getItem('theme'));    
    this.router.navigate(['qwe']); 
  }

  public clickInvokeButton(){
    console.log("нажата клавиша пополнить счёт");
    this.invokeClicked = 1;
  }

  public clickNotInvokeButton(){
    console.log("нажата клавиша снятие со счёта");
    this.invokeClicked = 2;
  }

  public chooseType(typeName: string){
    console.log("Тип операции выбран: " + typeName );
    this.typeForAction = typeName;
  }

  public doOperation(typeForAction: string, money: number, comment: string ){
    
    let token: string;

    if(localStorage.getItem('doSessionStorage') == 'session'){
      token = sessionStorage.getItem('MyToken');
    }
    else{
      token = localStorage.getItem('MyToken');
    }

    console.log("токен: " + token + " Тип: " + typeForAction + " денег: " + money + " Коммент: " + comment);

    this.moneyAccServ.doOperation(token, sessionStorage.getItem("MoneyGroupId"), typeForAction, money, comment);

    location.reload();
  }

}
